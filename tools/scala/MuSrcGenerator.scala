package tools.scala

import java.io.File
import java.nio.file.{Files, Paths, StandardOpenOption}

import cats.effect.{IO, Resource, Sync}
import cats.syntax.functor._
import com.google.protobuf.DescriptorProtos
import com.google.protobuf.DescriptorProtos.FileDescriptorSet
import higherkindness.mu.rpc.idlgen.Model.UseIdiomaticEndpoints
import higherkindness.mu.rpc.idlgen.proto._
import higherkindness.mu.rpc.idlgen._
import higherkindness.mu.rpc.idlgen.SrcGenApplication
import higherkindness.mu.rpc.internal.util.FileUtil._
import higherkindness.skeuomorph.mu.{CompressionType, MuF}
import higherkindness.skeuomorph.protobuf.{ParseProto, ProtobufF, Protocol}
import org.log4s._
import qq.droste.data.Mu
import qq.droste.data.Mu._

import scala.util.matching.Regex
import scala.collection.JavaConverters._

// Main class forked from source:
// https://github.com/higherkindness/mu/blob/master/modules/idlgen/core/src/main/scala/higherkindness/mu/rpc/idlgen/SrcGenApplication.scala
// The source at the time did not have a main class, but a class with an "apply()" method which
// produced a main.
object BazelProtoSrcGenerator {

  private[this] val logger = getLogger

  val idlType: String = proto.IdlType

  def inputFiles(files: Set[File]): Seq[File] =
    files.filter(_.getName.endsWith(ProtoExtension)).toSeq

  def generateFrom(
    protoDescFiles: Seq[String]
  ): Seq[(String, Seq[String])] =
    getCode[IO](protoDescFiles).unsafeRunSync

  def withImports(others: List[String])(self: String): String =
    (self.split("\n", 2).toList match {
      case h :: t => imports(h) :: others ++ t
      case a => a
    }).mkString("\n")

  val copRegExp: Regex = """((Cop\[)(((\w+)(\s)?(\:\:)(\s)?)+)(TNil)(\]))""".r

  val cleanCop: String => String =
    _.replace("Cop[", "").replace("::", ":+:").replace("TNil]", "CNil")

  val withCoproducts: String => String = self =>
    copRegExp.replaceAllIn(self, m => cleanCop(m.matched))

  val parseProtocol: Protocol[Mu[ProtobufF]] => higherkindness.skeuomorph.mu.Protocol[Mu[MuF]] =
    higherkindness.skeuomorph.mu.Protocol.fromProtobufProto(CompressionType.Identity, UseIdiomaticEndpoints(true))

  val printProtocol: higherkindness.skeuomorph.mu.Protocol[Mu[MuF]] => String =
    higherkindness.skeuomorph.mu.print.proto.print

  private def getCode[F[_] : Sync](protoDescFiles: Seq[String]): F[Seq[(String, Seq[String])]] = {
    val fdss: List[FileDescriptorSet] = protoDescFiles.map(f => FileDescriptorSet.parseFrom(Files.newInputStream(Paths.get(f)))).toList
    val fdps: List[DescriptorProtos.FileDescriptorProto] = fdss.flatMap(_.getFileList.asScala.toList)

    val protos = fdps.map(fdp => {
      val name = fdp.getName
        .replaceAllLiterally("/", "")
        .replaceAllLiterally(".", "")
      val marshallers = Seq(
        s"object ${name}Thing {\n" +
          """
            |implicit def implForMessage[T <: GeneratedMessage with Message[T]](
            |  implicit companion: GeneratedMessageCompanion[T]
            |)  = scalapb.grpc.Marshaller.forMessage
          """.stripMargin +
          s"\n}\nimport ${name}Thing._"
      )
        .toList
      (ParseProto.fromProto(fdp.getName, fdps), marshallers)
    })

    Sync[F].delay(
      protos
        .map { case (p, marshallers) =>
          getPath(p) ->
            Seq(
              (parseProtocol andThen printProtocol andThen withImports(marshallers) andThen withCoproducts)
                (p.copy(
                  name = protocolToName(p.name),
                  declarations = Nil,
                  imports = Nil
                ))
            )
        }
    )
  }

  private def protocolToName(p: String) = underscoreToCamel(p.split('/').last).capitalize

  private def underscoreToCamel(name: String) = "_([a-z\\d])".r.replaceAllIn(name, { m =>
    m.group(1).toUpperCase()
  }) + "Grpc"

  private def getPath(p: Protocol[Mu[ProtobufF]]): String =
    s"${p.name.split('/').last}_proto.scala"

  def imports(pkg: String): String =
    s"$pkg\nimport scalapb._\nimport higherkindness.mu.rpc.protocol._\nimport fs2.Stream\nimport shapeless.{:+:, CNil}\n\n"

}

object MuSrcGenerator {

  import java.util.logging.Logger

  private val logger = Logger.getLogger("MuSrcGenerator")

  def main(args: Array[String]) {
    val packageName = args(0)
    val protoDescFiles = args(1).split(",").toSeq
    println(s"Usage: ${args(0)} ${args(1)}")

    BazelProtoSrcGenerator.generateFrom(
      protoDescFiles
    ).foreach { case (outputPath, output) =>
      val path = s"$packageName/$outputPath"
      val f = new File(path)
      Option(f.getParentFile).foreach(_.mkdirs())
      f.write(output)
    }

  }
}
