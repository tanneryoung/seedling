load("@io_bazel_rules_scala//scala:scala.bzl", "scala_library")
load("@io_bazel_rules_scala//scala_proto:scala_proto.bzl", "scalapb_proto_library")

def scalamu_proto_library(name, deps):
    _scalamu_proto_library(
        name = name + "_scala_srcs",
        deps = deps,
    )
    scalapb_proto_library(
        name = name + "_proto_defs",
        visibility = ["//visibility:public"],
        deps = deps,
    )
    scala_library(
        name = name,
        srcs = [name + "_scala_srcs"],
        scalacopts = [
            "-Ypartial-unification",
            "-Xplugin:paradise_2.12.6.jar",
        ],
        plugins = [
            "//3rdparty/jvm/org/scalamacros:paradise_2_12_6",
        ],
        visibility = ["//visibility:public"],
        deps = [
            ":" + name + "_proto_defs",
            "//3rdparty/jvm/co/fs2:fs2_core",
            "//3rdparty/jvm/com/thesamet/scalapb:scalapb_runtime_grpc",
            "//3rdparty/jvm/com/chuusai:shapeless",
            "//3rdparty/jvm/io/higherkindness:mu_common",
            "//3rdparty/jvm/io/higherkindness:mu_rpc_fs2",
            "//3rdparty/jvm/io/higherkindness:mu_rpc_channel",
        ],
    )

def _impl(ctx):
    trans_proto_bins = depset(
        transitive = [dep[ProtoInfo].transitive_descriptor_sets for dep in ctx.attr.deps],
    )

    # Output always end in `-descriptor-set.proto.bin` so remove last 25 characeters
    outs = [
        ctx.actions.declare_file(ctx.attr.name + "/" + f.basename[0:len(f.basename) - 25] + ".scala")
        for f in trans_proto_bins.to_list()
    ]

    ctx.actions.run(
        inputs = trans_proto_bins,
        outputs = outs,
        arguments = [
            ctx.bin_dir.path + "/" + ctx.label.package + "/" + ctx.attr.name,
            ",".join([t.path for t in trans_proto_bins.to_list()]),
        ],
        progress_message = "Running for %s " % outs,
        executable = ctx.executable._mu_src_gen,
    )
    return DefaultInfo(files = depset(outs))

_scalamu_proto_library = rule(
    implementation = _impl,
    attrs = {
        "deps": attr.label_list(),
        "_mu_src_gen": attr.label(
            executable = True,
            cfg = "host",
            allow_files = True,
            default = Label("//tools/scala:mu_src_gen"),
        ),
    },
    provides = [DefaultInfo],
)
