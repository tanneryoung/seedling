#!/bin/bash

# This script will update the dependencies for the project.
# Should be run from the project root directory.

# This uses the git root as the project root, which will only work if run
# from within the project
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR=${SCRIPT_DIR%/tools/bazel_deps*}

REPO_SRC=https://github.com/johnynek/bazel-deps.git
REPO_DIR=${PROJECT_DIR:?}/tools/bazel_deps/bazel-deps/

mkdir -p ${REPO_DIR:?}
cd ${REPO_DIR:?}

if [[ ! $(ls -A ${REPO_DIR:?}) ]]
then
  git clone ${REPO_SRC:?} ${REPO_DIR:?}
else
  git pull ${REPO_SRC:?}
fi

bazel run //:parse generate -- \
  --repo-root ${PROJECT_DIR:?} \
  --sha-file 3rdparty/workspace.bzl \
  --deps tools/bazel_deps/dependencies.yml


