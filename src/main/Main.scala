package src.main

import java.util.logging.Logger

import cats.effect._
import cats.implicits._
import higherkindness.mu.rpc.server.interceptors.implicits._
import higherkindness.mu.rpc.server.metrics.MetricsServerInterceptor
import higherkindness.mu.rpc.server.{AddService, GrpcServer, UseTransportSecurity}
import src.main.hello._
import src.main.hello.HelloWorldServiceGrpc._
import src.main.interceptors.LoggingInterceptor

import scala.concurrent.ExecutionContext

object Main extends IOApp {
  private val ec: ExecutionContext = ExecutionContext.Implicits.global
  private implicit val ce: ConcurrentEffect[IO] = IO.ioConcurrentEffect(contextShift)

  private val logger: Logger = Logger.getLogger("Main")

  override def run(args: List[String]): IO[ExitCode] = {
    val loggingInterceptor = new LoggingInterceptor(logger)

    implicit val helloWorldServiceHandler: HelloWorldService[IO] = new HelloWorldServiceImpl()

    for {
      helloWorldService <- HelloWorldService.bindService[IO]
      services = (helloWorldService :: Nil).map(_
        .interceptWith(loggingInterceptor)
      ).map(AddService)

      config <- GrpcServer.netty[IO](8090, services)
      runServer <- GrpcServer.server[IO](config).as(ExitCode.Success)
    } yield runServer
  }
}
