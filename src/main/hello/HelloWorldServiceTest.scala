package src.main.hello

import org.scalatest._
import cats.effect.{ContextShift, IO}

import scala.concurrent.ExecutionContext

class HelloWorldServiceTest extends WordSpec
  with Matchers {

  val ec = ExecutionContext.global
  implicit val cs: ContextShift[IO] = IO.contextShift(ec)

  "HelloWorldService" when {
    "SayHello" should {
      "say hello given a name" in {
        val instance = new HelloWorldServiceImpl()

        (for {
          response <- instance.SayHello(SayHelloRequest(Some("my_name")))
          _ = response.message shouldEqual Some("Hello my_name!")
        } yield ()).unsafeRunSync
      }
    }
  }

}

