package src.main.hello

import cats.effect.IO
import src.main.hello.HelloWorldServiceGrpc.HelloWorldService

class HelloWorldServiceImpl extends HelloWorldService[IO] {
  def required[T](option: Option[T], message: String): IO[T] = {
    option match {
      case Some(t) => IO.pure(t)
      case None => IO.raiseError[T](new Exception(message))
    }
  }

  override def SayHello(req: SayHelloRequest): IO[SayHelloResponse] = for {
    name <- required(req.name, "Field `name` must be filled out.")
  } yield SayHelloResponse(Some(s"Hello $name!"))
}
