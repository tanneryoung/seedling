package src.main.interceptors

import java.util.logging.Logger

import io.grpc.ForwardingServerCall.SimpleForwardingServerCall
import io.grpc.ForwardingServerCallListener.SimpleForwardingServerCallListener
import io.grpc.{Metadata, ServerCall, ServerCallHandler, Status}

class LoggingInterceptor(val logger: Logger) extends io.grpc.ServerInterceptor {
  override def interceptCall[ReqT, RespT](call: ServerCall[ReqT, RespT], headers: Metadata, next: ServerCallHandler[ReqT, RespT]): ServerCall.Listener[ReqT] = {
    val nextCall = new SimpleForwardingServerCall[ReqT, RespT](call) {
      override def sendMessage(resp: RespT): Unit = {
        logger.info(s"Response: ${call.getMethodDescriptor.getFullMethodName}, Message: $resp")
        super.sendMessage(resp)
      }

      override def close(status: Status, trailers: Metadata): Unit = {
        logger.info(s"Response: ${call.getMethodDescriptor.getFullMethodName}, Status: $status, Cause: ${Option(status.getCause).map(_.getMessage)}")
        super.close(status, trailers)
      }
    }

    val listener: ServerCall.Listener[ReqT] = next.startCall(nextCall, headers)
    new SimpleForwardingServerCallListener[ReqT](listener) {
      override def onMessage(message: ReqT): Unit = {
        logger.info(s"Request: ${call.getMethodDescriptor.getFullMethodName}, Message: $message")
        super.onMessage(message)
      }

      override def onHalfClose(): Unit = {
        try
          super.onHalfClose
        catch {
          case e: Throwable =>
            logger.info(s"Request: ${call.getMethodDescriptor.getFullMethodName}, Error: ${e.getMessage}")
            call.close(Status.INTERNAL.withCause(e).withDescription("error message"), new Metadata)
        }
      }
    }
  }
}
